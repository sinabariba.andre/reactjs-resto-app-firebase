import esgoodday from "../img/es-goodday.png";
import jeruk from "../img/jeruk.png";
import NasiCampur from "../img/nasi-campur.png";
import SayurLodeh from "../img/sayurlodeh.png";

export const heroData = [
  {
    id: 1,
    name: "Drink",
    decp: "Capucino Italiano",
    price: "5.25",
    imageSrc: esgoodday,
  },
  {
    id: 2,
    name: "Oranges",
    decp: "Fresh Oranges",
    price: "10.25",
    imageSrc: jeruk,
  },
  {
    id: 3,
    name: "Rice",
    decp: "Mixed Rice Plate",
    price: "8.25",
    imageSrc: NasiCampur,
  },
  {
    id: 4,
    name: "Fish Kebab",
    decp: "Mixed Fish Kebab",
    price: "5.25",
    imageSrc: SayurLodeh,
  },
];

export const categories = [
  {
    id: 1,
    name: "Chicken",
    urlParamName: "chicken",
  },
  {
    id: 2,
    name: "Curry",
    urlParamName: "curry",
  },
  {
    id: 3,
    name: "Rice",
    urlParamName: "rice",
  },
  {
    id: 4,
    name: "Fish",
    urlParamName: "fish",
  },
  {
    id: 5,
    name: "Fruits",
    urlParamName: "fruits",
  },
  {
    id: 6,
    name: "Icecreams",
    urlParamName: "icecreams",
  },

  {
    id: 7,
    name: "Soft Drinks",
    urlParamName: "drinks",
  },
];
