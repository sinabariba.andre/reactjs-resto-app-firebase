import { getApp, getApps, initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
const firebaseConfig = {
  apiKey: "AIzaSyD08ZP9d83Bq5BioRDrAvgSLFEqIQA1i3U",
  authDomain: "reactjs-commerce-app.firebaseapp.com",
  databaseURL: "https://reactjs-commerce-app-default-rtdb.firebaseio.com",
  projectId: "reactjs-commerce-app",
  storageBucket: "reactjs-commerce-app.appspot.com",
  messagingSenderId: "857639225255",
  appId: "1:857639225255:web:6369aa8addaed5b53e1882",
};

const app = getApps.length > 0 ? getApp() : initializeApp(firebaseConfig);
const firestore = getFirestore(app);
const storage = getStorage(app);

export { app, firestore, storage };
