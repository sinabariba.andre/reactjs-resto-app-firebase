import React from "react";
import Delivery from "../img/delivery.png";
import HeroBg from "../img/herobg.jpg";
import { heroData } from "../utils/data";
import { motion } from "framer-motion";

const HomeContainer = () => {
  return (
    <section className="grid grid-cols-1 md:grid-cols-2 gap-2 w-full" id="home">
      <div className="py-2 flex-1 flex flex-col items-start justify-center gap-6">
        <div className="flex items-center gap-2 justify-center bg-orange-200 px-2 py-1 rounded-full">
          <p className="text-base text-cartNumBg font-semibold">
            Ojol delivery
          </p>
          <div className="w-6 h-6 bg-white rounded-full overflow-hidden drop-shadow-xl">
            <img
              src={Delivery}
              className="w-full h-full object-contain"
              alt="delivery"
            />
          </div>
        </div>
        <p className="text-[2.5rem] lg:text-[4.25rem] font-bold tracking-wide text-headingColor">
          The Fastest Delivery in
          <span className="text-cartNumBg text-[3rem] lg:text-[5rem]">
            Your City
          </span>
        </p>
        <p className="text-textColor text-base text-center md:text-left md:w-[90%]">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui eos
          aliquid fugiat tempora accusantium a tenetur asperiores quidem rerum
          sequi consequatur, amet quasi quos fugit magni at error incidunt. Amet
          iste sunt est consequatur quae libero reiciendis adipisci, quidem
          eveniet earum ut commodi ipsa atque nisi, aspernatur incidunt. Error,
          vitae.
        </p>
        <motion.button
          whileTap={{ scale: 0.9 }}
          type="button"
          className="w-full rounded-lg bg-cartNumBg hover:drop-shadow-lg px-4 py-2  md:w-auto "
        >
          Order Now
        </motion.button>
      </div>
      <div className="py-2 flex-1 flex items-center relative ">
        <img
          src={HeroBg}
          className="ml-auto w-full h-420 lg:w-auto lg:h-650 bg-center bg-200% rounded-3xl rotate-180 "
          alt="hero-bg"
        />
        <div className="w-full h-full absolute top-0 left-0 flex items-center justify-center xl:px-32 py-4 gap-4 drop-shadow-lg flex-wrap">
          {heroData &&
            heroData.map((el) => (
              <div
                key={el.id}
                className="lg:w-190 h-[180px] p-4 bg-cardOverlay backdrop-blur-md rounded-3xl flex flex-col items-center justify-end"
              >
                <div className="w-20 lg:w-40 -mt-10 lg:-mt-20">
                  <img src={el.imageSrc} className="object-contain" alt="I1" />
                </div>
                <p className="font-semibold text-textColor text-base lg:text-xl mt-2 lg:mt-4">
                  {el.name}
                </p>
                <p className="font-semibold text-orange-800 text-[12px] lg:text-sm my-1 lg:my-3">
                  {el.decp}
                </p>
                <p className="text-sm font-semibold text-headingColor">
                  <span className="text-xs text-cartNumBg">$</span>
                  {el.price}
                </p>
              </div>
            ))}
        </div>
      </div>
    </section>
  );
};

export default HomeContainer;
